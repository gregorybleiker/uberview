---
to: src/components/<%= name %>.ts
---
import { Component } from 'vue-property-decorator';
import Vue from 'vue';

@Component({
  components: { }
})
export class <%= name %> extends Vue {
}

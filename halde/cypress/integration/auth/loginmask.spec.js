import * as settings from '../../../settings.development';

describe('Login mask', () => {
  it('test user can sign in', () => {
    cy.visit(`${settings.uberview_endpoint}`);
    cy.get('#username-input').type('test');
    cy.get('#password-input').type('test');
    cy.get("#login-button").click();
    cy.get("#currentUser-div").then(result => {
      let user = result[0].innerText;
      expect(user).to.equal("test");
    });
  });
});

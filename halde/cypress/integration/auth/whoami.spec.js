import * as settings from '../../../settings.development';

describe('auth functions', () => {
    it('auth answers on whoami', () => {
        cy.request(`${settings.auth_endpoint}whoami`).then(response => {
          let userobject = response.body;
          expect(userobject).to.have.property('username');
        });
    });
});

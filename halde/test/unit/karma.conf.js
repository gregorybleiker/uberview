var merge = require('webpack-merge')
var testConfig = require('../../build/webpack.test.conf')
var webpack = require('webpack')
module.exports = function(config) {
    config.set({
        files: [{
            pattern: "./test.ts"
        }, ],
        preprocessors: {
            "./test.ts": ["webpack"], // *.tsx for React Jsx 
        },
        reporters: ["progress","html","mocha"],
        htmlReporter: {
           outputDir: 'karma_html', // where to put the reports  
           templatePath: null, // set if you moved jasmine_template.html 
           focusOnFailures: true, // reports show failures on start 
           namedFiles: false, // name files instead of creating sub-directories 
           pageTitle: null, // page title for reports; browser info by default 
           urlFriendlyName: false, // simply replaces spaces with _ for files/dirs 
           reportName: 'report-summary-filename', // report summary filename; browser info by default 
	},
        mochaReporter: {
            colors: {
                success: 'blue',
                info: 'bgGreen',
                warning: 'cyan',
                error: 'bgRed'
            },
            symbols: {
                success: '+',
                info: '#',
                warning: '!',
                error: 'x'
            }
        },
        singleRun: true,
        webpack: testConfig,
        webpackMiddleware: {
            noInfo: false
        },
        browsers: ["Chrome"],
        frameworks: ["mocha", "sinon-chai"],
        mime: {
            "text/x-typescript": ["ts"]
        }
    });
};

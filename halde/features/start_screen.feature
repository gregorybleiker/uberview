Feature: Start screen

  Scenario: Visit starting page
    Given Jeff opens a browser 
    When he navigates to the uberview homepage 
    Then he sees a blue bar 
    And a search field 
    And a graph section below the blue bar
    And the uberview logo

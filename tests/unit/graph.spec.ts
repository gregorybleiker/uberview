import { shallowMount } from "@vue/test-utils";
import Graph from "@/components/Graph.vue";

describe("Graph.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(Graph, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});

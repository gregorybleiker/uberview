# DEVPATH

## next tasks
currently:
- improve edit: make more fields addable
next:
- make tests work again
- INodeViewModelInterface: fix casing
- Remove already selected tag in context menu

## TODOS

### login

- need to restrict the amount of new logins per ip

## timeline

## 20.11.2019

trying remove tag
old tags a,b,c
new tags b,c,d
=> remove a
=> add d

## 8.10.2019

- https redirect problems in chrome
- trying to remove right click default menu

## Adding a foxx endpoint

- add route in foxx/api/APP/main.js
- add file in foxx/api/APP/routes.js

deploy with setup/copy_foxx.sh (maybe need to adjust path)

## 31.7.2019

debugging why username is not set in foxx code, done
"default" user is not seeded, only "test"
used swagger ui

```
curl -X POST "http://localhost:8529/_db/uberbase/uberauth/logup" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjcxNDIzMjQsImlhdCI6MS41NjQ1NTAzMjQzMjIzOTJlKzYsImlzcyI6ImFyYW5nb2RiIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicm9vdCJ9.RKwbza34jEKhTYHzxAlNjRgQrRcaGRJ-5FctZK6coLU=" -d "{ \"username\": \"default\", \"password\": \"something\"}"
```

insert is setting user_relations, good

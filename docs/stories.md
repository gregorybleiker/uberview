# info-path
a user should be able to click on a node and then swipe to a connected node. uberview will then keep the initially selected node selected and also select the swiped-to node. as soon as the swiped-to node is reached, it expands its children and the user can swipe to the next node. and so on.

# calculated stuff
on a given node, it is possible to calculate the value of some property based on the connected nodes. the calculation can be made either out of the visible nodes or out of the db nodes.

# bookmark graph
the current graph can be marked to be saved. this bookmark will then be accessible through a list. loading the bookmark will bring out the graph identical to the one that was bookmarked. if the model has changed in the meantime, the graph should still be loaded identically, but some options should be given as what should happen with the mismatch (recreate in model, delete from graph etc.)

# tagging
a group of nodes should be select-able. all nodes from this groups should be tag-able with a tag. Additionaly, links (tag-object relations) should be taggable as well. this will allow a certain constellation to be tagged and recalled with a single tag (e.g. choose "select all", tag with transport-overview)

# fuzzy operator
a fuzzy operator should be available (for example ~) such that tags with the fuzzy operator are matched in a fuzzy way. A fuzzy operator will cause a small slider or similar to appear after the first search in order to adjust the fuzziness.

# complexes
a set of tags and objects should be grouped into complexes. Complexes should be able to brought in to scope and also removed again for queries. For example, in initial query could be in the complex "Bookmarks", and after the result is displayed, the context "Projects" is activated. The query is then rerun but on the both datasets and the results updated. Optionally, this could be activated only for certain nodes.


module.exports = {
  pluginOptions: {
    quasar: {
      framework: {
        all: "auto"
      },
      extras: ["roboto-font", "material-icons"],
      treeShake: true,
      importStrategy: "kebab",
      rtlSupport: false
    }
  },

  transpileDependencies: ["quasar"]
};

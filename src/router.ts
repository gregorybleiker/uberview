import About from "./views/About.vue";
import DefaultLayout from "./layouts/Default.vue";
import Router from "vue-router";
import Uberview from "./views/Uberview.vue";
import Vue from "vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      children: [
        {
          component: Uberview,
          name: "home",
          path: ""
        },
        {
          component: About,
          name: "about",
          path: "/about"
        }
      ],
      component: DefaultLayout,
      path: "/"
    }
  ]
});

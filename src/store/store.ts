import axios from "axios";
import {} from "cytoscape";
import Vue from "vue";
import Vuex from "vuex";
import ConvertToGraphData from "../lib/ConvertToGraphData";
import * as Converters from "../models/Converters";
import { NodeViewModel } from "../models/NodeViewModel";
import { ITaggedResponse } from "../models/TaggedResponse";
import { INodeElementDto } from "@/models/UberDtos";

// let conf = require(`../../settings.${DEV ? 'development' : 'production'}.js`);

Vue.use(Vuex);

export default new Vuex.Store({
  actions: {
    async loadNodes(context) {
      if (context.state.query !== "") {
        await sendLoadNodes(context);
      }
    },
    addOrUpdateNode(context, node: NodeViewModel) {
      sendAddOrUpdateNode(
        context,
        Converters.NodeViewModelToNodeElementDto(
          node,
          context.state.currentUser
        )
      );
      sendLoadNodes(context);
    },
    async logup({ commit }, cred) {
      const result = await axios.post(
        `${process.env.VUE_APP_AUTHENDPOINT}logup`,
        {
          password: cred.password,
          username: cred.username
        }
      );
      if (result.data.success) {
        commit("currentUserChanged", {
          sessionId: result.headers["x-foxxsessionid"],
          username: cred.username
        });
      }
    },
    async whoami(context) {
      const result = await axios.get(
        `${process.env.VUE_APP_AUTHENDPOINT}whoami`,
        {
          headers: { "x-foxxsessionid": context.state.sessionId }
        }
      );
      context.commit("currentUserChanged", result.data.username);
    }
  },
  mutations: {
    updateLocalNodes(state, payload: ITaggedResponse) {
      state.graphData = ConvertToGraphData(payload, state);
    },
    queryChanged(state, payload) {
      state.query = payload;
    },
    currentUserChanged(state, payload) {
      state.currentUser = payload.username ? payload.username : "";
      state.sessionId = payload.sessionId;
    },
    updateUsername(state, payload: string) {
      state.currentUser = payload;
    }
  },
  state: {
    currentUser: "test",
    graphData: [],
    query: "",
    sessionId: ""
  }
});

async function sendAddOrUpdateNode(context, node: INodeElementDto) {
  try {
    await axios.post(
      `${process.env.VUE_APP_APIENDPOINT}upsertNode`,
      JSON.stringify(node)
    );
    console.log("saved node");
  } catch (error) {
    console.log("found error: " + error);
  }
}

async function sendLoadNodes(context) {
  try {
    const response = await axios.get(
      `${process.env.VUE_APP_APIENDPOINT}query`,
      {
        params: {
          query: JSON.stringify(context.state.query),
          username: context.state.currentUser
        }
      }
    );
    context.commit("updateLocalNodes", response.data);
  } catch (error) {
    console.log(error);
  }
}

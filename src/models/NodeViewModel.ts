import Vue from "vue";

export class NodeContentViewModel {
  public label: string = "";
  public value: string = "";
  public type: string = "";
}

export interface INodeViewModelInterface {
  name: string;
  Contents: NodeContentViewModel[];
  tags: string[];
}

export class NodeViewModel implements INodeViewModelInterface {
  get name(): string {
    if (this.privateName === "") {
      this.privateName = uuidv4();
    }
    return this.privateName;
  }

  set name(theName: string) {
    this.privateName = theName;
  }
  // name = "";
  public Contents = [new NodeContentViewModel()];
  /*
  get Contents() {
    return this.contents.filter(
      o => o.label && o.label.toLowerCase() !== "title"
    );
  }
  set Contents(in_content) {
    this.contents = in_content;
  }
  */
  public AddContent(content: NodeContentViewModel) {
    this.Contents.push(content);
    Vue.set(this, "Contents", this.Contents);
  }
  public tags = [];
  setTitle(title) {
    let t = this.Contents.find(
      o => o.label && o.label.toLowerCase() === "title"
    );
    if (t) {
      t.value = title;
      t.type = "text";
    } else {
      this.Contents.push({ label: "title", value: title, type: "text" });
    }
  }
  private privateName: string = "";
}

function uuidv4() {
  return (
    1e7 +
    "-" +
    1e3 +
    "-" +
    4e3 +
    "-" +
    8e3 +
    "-" +
    1e11
  ).replace(/[018]/g, (c: any) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}

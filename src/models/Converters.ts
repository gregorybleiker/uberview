import { NodeContentViewModel, NodeViewModel } from "../models/NodeViewModel";
import {
  IEdgeElementDto,
  INodeElementDto,
  ITagElementDto
} from "../models/UberDtos";

import { IUberNode } from "../models/UberNode";

export function StringToNode(tag: string) {
  return {
    data: {
      id: tag,
      type: "tag"
    }
  };
}

export function TagToNode(tag: ITagElementDto) {
  return {
    data: {
      id: tag,
      type: "tag"
    }
  };
}

export function EdgeToNode(edge: IEdgeElementDto) {
  return {
    data: {
      id: edge.name,
      source: edge.source,
      target: edge.target
    }
  };
}

export function NodeToNode(node: INodeElementDto) {
  let safeTitle = "";
  if (node.contents.find) {
    const titleContent = node.contents.find(
      o => o.label && o.label.toLowerCase() === "title"
    );
    if (titleContent) {
      if (titleContent.value) {
        safeTitle = titleContent.value;
      }
    }
  }
  const data = ({
    contents: node.contents,
    id: node.name,
    label: safeTitle,
    name: node.name,
    tags: node.tags,
    type: "node"
  } as any) as IUberNode;

  return { data };
}

export function NodeViewModelToNodeElementDto(
  node: NodeViewModel,
  username: string
): INodeElementDto {
  return {
    contents: node.Contents.filter(o => {
      return o.label && o.type && o.value;
    }),
    name: node.name,
    tags: node.tags,
    username
  };
}

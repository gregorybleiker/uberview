import {
  IEdgeElementDto,
  INodeElementDto,
  ITagElementDto
} from "../models/UberDtos";

export interface ITaggedResponse {
  tags: ITagElementDto[];
  edges: IEdgeElementDto[];
  nodes: INodeElementDto[];
}

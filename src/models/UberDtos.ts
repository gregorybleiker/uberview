export interface INodeContentDto {
  label: string;
  value: string;
  type: string;
}

export interface INodeElementDto {
  name: string;
  contents: INodeContentDto[];
  tags: string[];
  username: string;
}

export interface ITagElementDto {
  name: string;
}

export interface IEdgeElementDto {
  name: string;
  source: string;
  target: string;
}

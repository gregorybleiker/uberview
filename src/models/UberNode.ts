import { INodeContentDto } from "./UberDtos";

export interface IUberNode {
  label: string;
  name: string;
  contents: INodeContentDto[];
  tags: string[];
}

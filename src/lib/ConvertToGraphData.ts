// import GetTagIntersection from "./GetTagIntersection";
import * as Converters from "../models/Converters";
import * as parser from "./Parser";

export default function(documents, state) {
  const graphData = [];
  // let tags = GetTagIntersection(documents);
  const rawTags = parser.parse(state.query).split(",") as string[];
  let tags = rawTags.map(o => (o = o.trim()));
  for (let tag of tags) {
    graphData.push(Converters.StringToNode(tag));
  }

  for (const doc of documents._documents[0][0]) {
    const graphNode = Converters.NodeToNode(doc);
    graphData.push(graphNode);
    for (let tag of graphNode.data.tags) {
      if (tags.includes(tag)) {
        const data: any = {};
        data.id = `${tag}_to_${doc.name}`;
        data.source = tag;
        data.target = doc.name;
        graphData.push({ data });
      }
    }
  }
  return graphData;
}

import { NodeViewModel } from "../models/NodeViewModel";

export default function CommandsForNode(self) {
  return ele => {
    const result = [
      {
        content: "edit",
        enabled: true,
        fillColor: "rgba(100,100,100,0.75)",
        select(element) {
          const vm = new NodeViewModel();

          if (element.data !== undefined && element.data().type === "node") {
            // vm.label = element
            //  .data()
            //  .contents.find(o => o.label === 'Title').content
            vm.Contents = element.data().contents;
            // .filter(o => o.label !== 'Title')
            vm.name = element.data().name;
            vm.tags = element.data().tags;
          } else {
            vm.name = "";
            vm.tags = [];
            vm.Contents = [];
          }
          self.AddEditViewModel = vm;
          // timeout is needed so that context menu does not show for right button click
          setTimeout(() => {
            self.showDialog = true;
          }, 100);
        }
      }
    ];
    if (ele._private.data.tags.length) {
      for (const tag of ele._private.data.tags) {
        const node = {
          content: tag,
          enabled: true,
          fillColor: "rgba(200, 200, 200, 0.75)",
          select() {
            self.$store.commit(
              "queryChanged",
              `${self.$store.state.query} OR ${tag}`
            );
            self.$store.dispatch("loadNodes");
          }
        };
        result.push(node);
      }
    }
    return result;
  };
}

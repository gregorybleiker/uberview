export default function(docs) {
  let result = [];
  if (docs._documents) {
    result = docs._documents[0][0].tags;
    for (const doc of docs._documents[0]) {
      result = result.filter(e => doc.tags.includes(e));
    }
  }
  return result;
}

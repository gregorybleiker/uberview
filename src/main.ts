import { Quasar } from "quasar";
import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store/store";

import "typeface-audiowide";
import "./styles/quasar.styl";
import "./styles/app.styl";
import "./quasar";

Vue.use(Quasar, {
  framework: {
    all: "auto"
  },
  extras: ["roboto-font", "material-icons"],
  treeShake: true
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store
}).$mount("#app");

const shell = require("shelljs"),
  path = require("path"),
  sleep = require("sleep"),
  variables = require("./variables.js"),
  fs = require("fs"),
  { program } = require("commander");

program
  .option("-a, --arangodb", "install arangodb docker image")
  .option("-c --copy-only", "copy files to 'install' folder")
  .option("-d --data", "initialize data")
  .option("-r --rest", "install the rest (split up later)")
  .parse(process.argv);

console.log(`base dir in ${variables.baseDir}`);
console.log(`Running installer in ${variables.installDir}`);

if (!fs.existsSync(`${variables.installDir}`)) {
  // -p also creates the parents
  shell.mkdir("-p", `${variables.installDir}`);
}
shell.cp(
  "-R",
  `${variables.setupDir}${path.sep}install${path.sep}*`,
  variables.installDir
);
shell.cp(
  `${variables.setupDir}${path.sep}variables.js`,
  `${variables.installDir}${path.sep}`
);
shell.cd(variables.installDir);
if (program.copyOnly) {
  process.exit();
}

if (program.arangodb) {
  if (
    !fs.existsSync(
      `${variables.installDir}${path.sep}docker_volumes${path.sep}arangodb3-apps`
    )
  ) {
    // -p also creates the parents
    shell.mkdir(
      "-p",
      `${variables.installDir}${path.sep}docker_volumes${path.sep}arangodb3-apps`
    );
  }
  console.log("Starting container...");
  shell.exec(`node ${variables.installDir}${path.sep}run_arangodb.js`);

  let id = "";
  for (i = 0; i < 4; i++) {
    sleep.sleep(15);
    id = shell
      .exec('docker ps  -f "ancestor=arangodb:latest" --format "{{.ID}}"')
      .stdout.replace("\n", "");
    if (id !== "") break;
  }

  // console.log('Restoring default data...')
  // shell.exec(`docker exec -td ${
  //     id} arangorestore --input-directory=/data/local1 --server.endpoint
  //     tcp://localhost:8529 --server.database=uberbase --server.username=${
  //     variables.dbUser} --server.password=${variables.dbPassword}`);
}

if (program.data) {
  console.log("Initializing data...");
  shell.exec(`node ${variables.installDir}${path.sep}init_data.js`);

  console.log("Initializing services...");
  shell.exec(`node ${variables.installDir}${path.sep}init_services.js`);

  console.log("Copying services...");
  shell.exec(`node ${variables.installDir}${path.sep}copy_services.js`);
}
if (program.rest) {
  if (!fs.existsSync(`${variables.installDir}${path.sep}certs`)) {
    shell.mkdir(`${variables.installDir}${path.sep}certs`);
    shell.mkdir(
      `${variables.installDir}${path.sep}certs${path.sep}uberview.io`
    );
    shell.exec(
      `openssl req -nodes -config ${variables.setupDir}${path.sep}cert.conf -x509 -newkey rsa:4096 -keyout ${variables.installDir}${path.sep}certs${path.sep}uberview.io${path.sep}privkey.pem -out ${variables.installDir}${path.sep}certs${path.sep}uberview.io${path.sep}cert.pem -days 365 -batch`
    );
  }
  if (!fs.existsSync(`${variables.installDir}${path.sep}.well-known`)) {
    shell.mkdir(`${variables.installDir}${path.sep}.well-known`);
  }

  sleep.sleep(10);

  console.log("building uberview");
  // shell.exec(`${variables.baseDir}${path.sep}node_modules${path.sep}quasar-cli${path.sep}bin${path.sep}quasar
  // build ${variables.type}`)
  shell.exec(`yarn install`);
  shell.exec(`yarn build`);

  sleep.sleep(10);

  console.log("Starting nginx...");
  shell.exec(
    `node ${variables.installDir}${path.sep}run_nginx.js ${variables.type}`
  );
}

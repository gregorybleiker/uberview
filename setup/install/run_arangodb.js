const variables = require('./variables.js');
const axios = require('axios');
const shell = require('shelljs');
const sleep = require('sleep');
const path = require('path');

async function doit() {
  console.log('checking for container')
  let id =
      shell
          .exec('docker ps -a -f "ancestor=arangodb:latest" --format "{{.ID}}"')
          .stdout;

  if (id !== '') {
    console.log('Starting container ' + id)
    shell.exec('docker start ' + id)
  } else {
    console.log('Creating new container' + id)
    let cmd = `docker run -p 8529:${
        variables.dbPort} --env ARANGO_ROOT_PASSWORD=${
        variables.dbPassword} --volume="${variables.forwardInstallDir}${
        path.sep}docker_volumes${
        path.sep}arangodb3-apps:/var/lib/arangodb3-apps" --volume="${
        variables
            .forwardBaseDir}/backup/data:/data" -td arangodb:latest arangod`;
    console.log(cmd);
    shell.exec(cmd);
  }

  let finished = false;
  for (i = 0; i < 30 && !finished; i++) {
    console.log('checking arangodb...');
    try {
      await axios.get(
          `http://${variables.dbHost}:${
              variables.dbPort}/_db/_system/_admin/aardvark/index.html`,
          {timeout: 1000});
      console.log('arangodb up');
      finished = true;
    } catch (error) {
      console.log('arangodb not up');
      sleep.sleep(5);
    }
  }
};
doit();
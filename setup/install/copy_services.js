let shell = require('shelljs')
let variables = require('./variables');

let uberbaseDir = `${variables.forwardInstallDir}/docker_volumes/arangodb3-apps/_db/uberbase/`;
let installDir = variables.forwardInstallDir;

if (process.platform.indexOf('win') !== 0) {
    console.log("copying files for non-windows")

    shell.exec(`sudo mkdir -p ${uberbaseDir}uberview/`)
    shell.exec(`sudo mkdir -p ${uberbaseDir}api/`)
    shell.exec(`sudo cp -rf ${variables.baseDir}/foxx/uberview/* ${uberbaseDir}uberview/`)
    shell.exec(`sudo cp -rf  ${variables.baseDir}/foxx/api/* ${uberbaseDir}api/`)
    shell.exec(`sudo cp -rf  ${variables.baseDir}/foxx/uberauth/* ${uberbaseDir}uberauth/`)
    shell.exec(`sudo cp -rf ${variables.baseDir}/dist/* ${uberbaseDir}uberview/APP/static/`)
}

if (process.platform.indexOf('win') === 0) {
    console.log("copying files for windows")

    shell.mkdir('-p', `${uberbaseDir}uberview/`)
    shell.mkdir('-p', `${uberbaseDir}api/`)
    shell.cp('-rf', `${variables.baseDir}/foxx/uberview/*`, `${uberbaseDir}uberview/`)
    shell.cp('-rf', `${variables.baseDir}/foxx/api/*`, `${uberbaseDir}api/`)
    shell.cp('-rf', `${variables.baseDir}/foxx/uberauth/*`, `${uberbaseDir}uberauth/`)
    shell.cp('-rf', `${variables.baseDir}/dist/*`, `${uberbaseDir}uberview/APP/static/`)
}
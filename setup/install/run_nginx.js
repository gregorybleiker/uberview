const variables = require('./variables');

let shell = require('shelljs')
let path = require('path')
let arangoid = shell.exec('docker ps  -f "ancestor=arangodb:latest" --format "{{.ID}}"').stdout;
let id = shell.exec('docker ps  -f "ancestor=jwilder/nginx-proxy" --format "{{.ID}}"').stdout;
let ipcommand = `docker inspect -f "{{ .NetworkSettings.IPAddress }}" ${arangoid}`
let arangoip = shell.exec(ipcommand).stdout;
arangoip = arangoip.replace(/^\s+|\s+$/g, '');

let conf = require(`../settings.${process.argv[2]}.js`);
console.log(`conf ${conf}`)
let uberviewFile = path.resolve(`${variables.installDir}${path.sep}uberview.conf`)
console.log(arangoip)
if (id !== '') {
    console.log(`Starting container ${id}`)
    shell.exec(`docker start ${id}`)
} else {
    console.log('Creating new container')
    shell.sed('-i', /arango_ip/g, arangoip, uberviewFile)
    shell.sed('-i', /host_name/g, conf.host_name, uberviewFile)
    shell.exec(`docker run -p 80:80 -p 443:443 -e "ENABLE_IPV6=true" --volume "/var/run/docker.sock:/tmp/docker.sock:ro" --volume="${variables.installDir}/uberview.conf:/etc/nginx/conf.d/uberview.conf" --volume="${variables.installDir}/.well-known/:/var/well-known" --volume="${variables.installDir}/certs/:/var/certs/:ro" -d jwilder/nginx-proxy`)
}

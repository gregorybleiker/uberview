const variables = require('./variables');
const axios = require('axios');

async function createService(serviceName, serviceDescription) {
  // create api service

  let payload = {
    'name': `${serviceName}`,
    'documentCollections': [],
    'edgeCollections': [],
    'author': 'uberview',
    'license': 'MIT',
    'description': `${serviceDescription}`
  };

  await axios
      .put(
          `http://${variables.dbHost}:${
              variables
                  .dbPort}/_db/uberbase/_admin/aardvark/foxxes/generate?mount=%2F${
              serviceName}`,
          JSON.stringify(payload),
          {auth: {username: variables.dbUser, password: variables.dbPassword}})
      .then((response) => {
        console.log(`created service ${JSON.stringify(serviceName)}`);
      })
      .catch((error) => {
        console.log(`${serviceName} install error: ${error}`);
      });

  await axios
      .patch(
          `http://${variables.dbHost}:${
              variables
                  .dbPort}/_db/uberbase/_admin/aardvark/foxxes/devel?mount=%2F${
              serviceName}`,
          JSON.stringify(true),
          {auth: {username: variables.dbUser, password: variables.dbPassword}})
      .then((response) => {
        console.log(`configured service ${JSON.stringify(serviceName)}`);
      })
      .catch((error) => {
        console.log(`${serviceName} configuration error: ${error}`);
      });
}

async function postPayload(payload, database, location) {
  await axios
      .post(
          `http://${variables.dbHost}:${variables.dbPort}/_db/${
              database}/_api/${location}`,
          JSON.stringify(payload),
          {auth: {username: variables.dbUser, password: variables.dbPassword}})
      .then((response) => {
        console.log(`posted payload ${JSON.stringify(payload)}`);
      })
      .catch((error) => {
        console.log(`post error: ${error}`);
      });
}

async function putPayload(payload, database, location) {
  await axios
      .put(
          `http://${variables.dbHost}:${variables.dbPort}/_db/${
              database}/_api/${location}`,
          JSON.stringify(payload),
          {auth: {username: variables.dbUser, password: variables.dbPassword}})
      .then((response) => {
        console.log(`put payload ${JSON.stringify(payload)}`);
      })
      .catch((error) => {
        console.log(`put error: ${error}`);
      });
}
module.exports = {
  createService,
  postPayload,
  putPayload
};
const serviceHelpers = require('./servicehelpers');

async function doit() {

    //create api service
    serviceHelpers.createService('api', 'uberview backend service');
    //create uberview service
    serviceHelpers.createService('uberview', 'uberview spa server');
    //create uberauth service
    serviceHelpers.createService('uberauth', 'uberauth service');
}
doit();
const axios = require('axios');
// this will be copied to input, so no '../variables'
const variables = require('./variables');
const serviceHelpers = require('./servicehelpers');

async function doit() {
  let payload = {'name': 'uberbase'};
  await serviceHelpers.postPayload(payload, '_system', 'database');

  payload = {
    'name': 'contents',
    waitForSync: false,
    isSystem: false,
    type: 2,
    numberOfShards: 1,
    shardKeys: []
  };
  await serviceHelpers.postPayload(payload, 'uberbase', 'collection');

  payload = {
    'name': 'tags',
    waitForSync: false,
    isSystem: false,
    type: 2,
    numberOfShards: 1,
    shardKeys: []
  };
  await serviceHelpers.postPayload(payload, 'uberbase', 'collection');

  payload = {
    name: 'tagged',
    waitForSync: false,
    isSystem: false,
    type: 3,
    numberOfShards: 1,
    shardKeys: []
  };
  await serviceHelpers.postPayload(payload, 'uberbase', 'collection');

  payload = {
    name: 'user_relations',
    waitForSync: false,
    isSystem: false,
    type: 3,
    numberOfShards: 1,
    shardKeys: []
  };
  await serviceHelpers.postPayload(payload, 'uberbase', 'collection');

  payload = {
    name: 'uberauth_users',
    waitForSync: false,
    isSystem: false,
    type: 2,
    numberOfShards: 1,
    shardKeys: []
  };
  await serviceHelpers.postPayload(payload, 'uberbase', 'collection');

  payload = {'grant': 'rw'};
  await serviceHelpers.putPayload(
      payload, 'uberbase', 'user/root/database/uberbase');
}
doit();
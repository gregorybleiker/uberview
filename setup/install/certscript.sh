#!/bin/sh
# remove --staging flag if you want to do it for real
certbot certonly --webroot -w /root/uberview/install/ -d uberview.io -m gregorybleiker@gmail.com --agree-tos --eff-email --break-my-certs --staging

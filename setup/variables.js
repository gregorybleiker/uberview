let path = require("path");
let dbUser, dbPassword, dbHost, dbPort;
let type = process.argv[2] || "development";
if (type === "development") {
  dbUser = "root";
  dbPassword = "7stein";
  dbHost = "localhost";
  dbPort = "8529";
} else {
}
let setupDir = path.dirname(__filename);
let baseDir = path.resolve(`${setupDir}${path.sep}..`);
let installDir = path.resolve(`${baseDir}${path.sep}install${path.sep}`);
let forwardInstallDir = installDir.replace(/\\\\/g, "/");
let forwardBaseDir = baseDir.replace(/\\\\/g, "/");

if (installDir.indexOf("\\") !== 0) {
  installDir = installDir.replace(/\\/g, "\\\\");
}

module.exports = {
  type,
  dbUser,
  dbPassword,
  dbHost,
  dbPort,
  installDir,
  setupDir,
  forwardInstallDir,
  baseDir,
  forwardBaseDir
};

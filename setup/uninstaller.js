let
    shell = require('shelljs'),
    path = require('path'),
    sleep = require('sleep'),
    variables = require('./variables')
let id = '';
id = shell.exec('docker ps -a -f "ancestor=arangodb:latest" --format "{{.ID}}"').stdout.replace('\n', '');
if (id !== '') {
    console.log('Removing arangodb...')
    shell.exec(`docker rm -fv ${id}`)
}
id = '';
id = shell.exec('docker ps -a -f "ancestor=jwilder/nginx-proxy" --format "{{.ID}}"').stdout.replace('\n', '');

if (id !== '') {
    console.log('Removing nginx...')
    shell.exec(`docker rm -fv ${id}`)
}
console.log('removing install dir ' + variables.installDir)
if (process.platform.indexOf('win') !== 0) {
    shell.exec(`sudo rm -rf ${variables.installDir}`)
}
if (process.platform.indexOf('win') === 0) {
    shell.rm('-rf', variables.installDir)
}
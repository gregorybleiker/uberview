'use strict';
const _ = require('lodash');
const joi = require('joi');
const elements = require('./elements');

module.exports = {
  schema: elements.taggedResponse,
  forClient(obj) {
    // Implement outgoing transformations here
    obj = _.omit(obj, ['_id', '_rev', '_oldRev']);
    return obj;
  },
  fromClient(obj) {
    // Implement incoming transformations here
    if (typeof(obj.tags) === "string") {
      obj.tags = obj.tags.split(',');
    }
    return obj;
  }
};

'use strict'
const joi = require('joi')

const nodeQuerySchema = {
  name: joi.string()
}
const user = {
  username: joi.string()
}
const nodeContentSchema = {
  label: joi.string(),
  value: joi.string(),
  type: joi.string()
}

const nodeSchema = joi.object().keys({
  name: joi.string(),
  users: joi.array().items(user),
  contents: joi.array().items(nodeContentSchema),
  tags: joi.array().items(joi.string()),
  username: joi.string()
})

const edgeSchema = {
  name: joi.string(),
  source: joi.string(),
  tartget: joi.string()
}
const tagSchema = {
  name: joi.string()
}
const taggedResponse = {
  nodes: joi.array().items(nodeSchema),
  edges: joi.array().items(edgeSchema),
  tags: joi.array().items(tagSchema)
}
module.exports = {
  // Describe the attributes with joi here
  nodeQuerySchema,
  nodeContentSchema,
  nodeSchema,
  edgeSchema,
  tagSchema,
  taggedResponse
}

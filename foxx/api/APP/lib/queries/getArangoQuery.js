'use strict'
const parser = require('../queryParser.js')

module.exports = function getArangoQuery(username, query) {
  let parsedQuery = parser.parse(query)
  parsedQuery = parsedQuery.replace('THEUSERNAME', username)
  console.log(parsedQuery)
  return `let result = (${parsedQuery}) return result`
}

"use strict";
module.exports = {
  upsertNodeQuery: node => {
    let username = node.username || "default";
    return `
        let nodeid =  first(
           upsert { _key: "${node.name}" }
            insert { _key: "${node.name}", name: "${node.name}", contents: ${JSON.stringify(node.contents)} }
            update { name: "${node.name}", contents: ${JSON.stringify(node.contents)}  } 
        in contents
        let upserted = NEW return upserted._id)
        let user = first(for u in uberauth_users filter u.username == '${username}' limit 1 return u._id)
        let up =  (upsert { _from: user, _to: nodeid }
            insert { _from: user, _to: nodeid }
            update { _from: user, _to: nodeid }
            in user_relations)
        return nodeid 
        `;
  },
  upsertTagQueries: (node, nodeId) => {
    let queries = [];
    let username = node.username || "default";
    if (node.tags && node.tags.length > 0) {
      for (let tag of node.tags) {
        //add new tags
        queries.push(`
            let tagNodeId = first(upsert { name:"${tag}" }
            insert { name:"${tag}" }
            update { name:"${tag}" }
            in tags
            let newTag = NEW
            return newTag._id)
            let taggedNode = first(upsert {_from: tagNodeId  , _to: "${nodeId}" }
              insert {_from: tagNodeId  , _to: "${nodeId}" }
              update { } 
              in tagged
              return true)
            let user = first(for u in uberauth_users filter u.username == '${username}' limit 1 return u._id)
            upsert { _from: user, _to:tagNodeId }
            insert { _from: user, _to:tagNodeId }
            update { _from: user, _to:tagNodeId }
            in user_relations
            `);
      }
      //remove missing tags
      queries.push(`
let currentTags=(
for tag in inbound
    first(for c in contents filter c._id=='${nodeId}' limit 1 return {_id:c._id} ) tagged
    return tag.name
)
let both = intersection(currentTags, ${JSON.stringify(node.tags)})
let toremove = outersection(currentTags, both)
let removeTags = (
for t in tags
filter POSITION(toremove, t.name)
return t._id
)
for u in (
    for t in tagged 
    filter t._to == '${nodeId}'
    return t
    )
filter POSITION(removeTags, u._from)
remove u in tagged
            `);
    }
    return queries;
  }
};

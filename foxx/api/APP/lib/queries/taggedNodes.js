"use strict";
module.exports = function taggedNodes(tag) {
  return `
for content in outbound
first(for t in tags filter t.name=='${tag}' limit 1 return {_id:t._id} ) tagged
return {
    name: content.name,
    contents: content.contents,
    tags: (
        for t in inbound
        first(for c in contents filter c.name==content.name limit 1 return {_id:c._id})
        tagged return t.name)
}
`;
};

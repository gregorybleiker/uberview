'use strict';

function getQueryForSingleTag(tag) {
    return `(for content in outbound
    first(for t in tags filter t.name=='${tag}' limit 1 return {_id:t._id} ) tagged
    return {
      name: content.name,
      contents: content.contents,
      tags: (
        for t in inbound
        first(for c in contents filter c.name==content.name limit 1 return {_id:c._id})
        tagged return t.name)
    }
  ),`;
}

module.exports =
    function taggedNodes(tags) {
        let tagQueries = "";
        let totalQuery = "";
        if (tags !== undefined) {

            let graphData = {
                tags: [],
                edges: [],
                nodes: []
            };

            //tag nodes
            for (var tag of tags) {
                tagQueries = tagQueries.concat(getQueryForSingleTag(tag));
            }

            //tagged nodes
            //assemble tagged nodes query
            tagQueries = tagQueries.substring(0, tagQueries.length - 1);
            if (tags.length == 1) {
                totalQuery = tagQueries.substring(1, tagQueries.length - 1);
            } else {
                totalQuery = 'for node in intersection (' + tagQueries + ') return node';
            }
            console.log(totalQuery);
        }
        return totalQuery;
    }

"use strict";
const db = require("@arangodb").db;
const taggedNodesQuery = require("./queries/taggedNodes");
const upsertNodeQuery = require("./queries/upsertNode");
const getTaggedQuery = require("./queries/getTagged");
const getArangoQuery = require("./queries/getArangoQuery");
let dbaccess = {};

dbaccess.getNode = tagname => {
  let result = db._query(taggedNodesQuery(tagname));
  return result;
};

dbaccess.upsertNode = node => {
  db._executeTransaction({
    collections: {
      read: "uberauth_users",
      write: ["contents", "tagged", "tags", "user_relations"]
    },
    action: () => {
      const db = require("@arangodb").db;
      let query = upsertNodeQuery.upsertNodeQuery(node);
      let queryResult = db._query(query);
      if (queryResult._documents.length > 0) {
        let nodeId = queryResult._documents[0];
        console.log(JSON.stringify(nodeId));
        query = upsertNodeQuery.upsertTagQueries(node, nodeId);
        for (let subquery of query) {
          console.log(subquery);
          db._query(subquery);
        }
      }
    }
  });
};

dbaccess.getTagged = tags => {
  let result = db._query(getTaggedQuery(tags));
  return result;
};
dbaccess.query = function(username, query) {
  let result = db._query(getArangoQuery(username, query));
  return result;
};
module.exports = dbaccess;

"use strict";
const joi = require("joi");
const createRouter = require("@arangodb/foxx/router");
const dbaccess = require("../lib/api");
const nodelist = require("../models/nodelist");
const elements = require("../models/elements");
const router = createRouter();

module.exports = router;

router.options("/tagged", function(req, res) {
  res.headers["Access-Control-Allow-Credentials"] = true;
});

router
  .get("/tagged", function(req, res) {
    let queryTags = req.queryParams.tags;
    let result = dbaccess.getTagged(queryTags);
    res.json(result);
  })
  .queryParam("tags", joi.array().items(joi.string()))
  .response(nodelist);

router.options("/query", function(req, res) {
  res.headers["Access-Control-Allow-Credentials"] = true;
});
router
  .get("/query", function(req, res) {
    let queryParam = req.queryParams.query;
    let userName = req.queryParams.username;
    queryParam = queryParam.replace(/"/g, "");
    console.log(`Query param: ${queryParam}`);
    let result = {};
    if (queryParam) {
      result = dbaccess.query(userName, queryParam);
    }
    res.json(result);
  })
  .queryParam("query", joi.string().required())
  .queryParam("username", joi.string().required())
  .response(nodelist);

router
  .get("/getNode", function(req, res) {
    console.log("called getnode");
    let name = req.queryParams.name;
    let result = dbaccess.getNode(name);
    res.json(result);
  })
  .queryParam("name", joi.string())
  .response(nodelist);

router
  .post("/upsertNode", function(req, res) {
    // todo validate req.body for naughty things
    if (
      !req.body.name ||
      !req.body.contents ||
      !Array.isArray(req.body.contents) ||
      !req.body.contents.some(
        o =>
          o.label &&
          o.label.toLowerCase() == "title" &&
          o.type &&
          o.type.toLowerCase() == "text"
      )
    ) {
      res.throw(400, "Missing or malformed Title field");
    }
    if (
      !req.body.tags ||
      !Array.isArray(req.body.tags) ||
      req.body.tags.length === 0
    ) {
      res.throw(400, "Missing or malformed tags");
    }
    let result = dbaccess.upsertNode(req.body);
    res.json(result);
  })
  .body(elements.nodeSchema)
  .response(nodelist);

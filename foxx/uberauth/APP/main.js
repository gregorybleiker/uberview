"use strict";

const joi = require("joi");
const sessionsMiddleware = require("@arangodb/foxx/sessions");
const cookieTransport = require('@arangodb/foxx/sessions/transports/cookie');
const headerTransport = require('@arangodb/foxx/sessions/transports/header');
const createRouter = require("@arangodb/foxx/router");
const router = createRouter();
module.context.use(router);

const sessions = sessionsMiddleware({
  storage: module.context.collection("sessions"),
  transport: headerTransport('x-foxxsessionid')
/*
  transport: cookieTransport({
    name: 'FOXXSESSID',
    ttl: 60 * 60 * 24 * 7, // one week in seconds
    algorithm: 'sha256',
    secret: 'somethingsecret'
  })
*/
});
module.context.use(sessions);

const uberauth = require("./routes/uberauth");

// module.context.use('/uberauth', require('./routes/uberauth'))

router
  .get("/whoami", function(req, res) {
    let result = uberauth.whoami(req.session);
    res.send(result);
  })
  .description("Returns the currently active username.");

router
  .post("/logout", function(req, res) {
    uberauth.logout(req);
    res.send({
      success: true
    });
  })
  .description("Logs the current user out.");

router
  .post("/logup", function(req, res) {
    try {
      uberauth.logup(
        req.body.username,
        req.body.password,
        req.session,
        req.sessionStorage
      );
    } catch (e) {
      res.send(JSON.stringify(e));
      return;
    }
    res.send({ success: true });
  })
  .body(
    joi
      .object({
        username: joi.string().required(),
        password: joi.string().required()
      })
      .required(),
    "Credentials"
  )
  .description("Logs a registered user in or create a new one.");

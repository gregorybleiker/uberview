"use strict";

const createAuth = require("@arangodb/foxx/auth");

const auth = createAuth();
const users = module.context.collection("users");

module.exports = {
  whoami(session) {
    console.log(`session: ${JSON.stringify(session)}`);
    try {
      const user = users.document(session.uid);
      return {
        username: user.username
      };
    } catch (e) {
      return {
        username: null
      };
    }
  },
  logout(req) {
    if (req.session.uid) {
      req.session.uid = null;
      req.sessionStorage.save(req.session);
    }
  },
  logup(username, password, session, sessionStorage) {
    console.log("logup");
    // This may return a user object or null
    let user = users.firstExample({ username });
    console.log(`user: ${user}`);
    // create if not existing
    if (user === null) {
      try {
        // Create an authentication hash
        user = { username };
        console.log(`Saving user: ${username}`);
        user.authData = auth.create(password);
        console.log("checking user" + JSON.stringify(user));
        const meta = users.save(user);
        Object.assign(user, meta);
      } catch (e) {
        // Failed to save the user
        // We'll assume the UniqueConstraint has been violated
        console.log(e);
        res.throw("bad request", "failure during save", e);
      }
    }
    const valid = auth.verify(
      // Pretend to validate even if no user was found
      user.authData,
      password
    );
    if (!valid) {
      console.log("unauthorized");
      res.throw("unauthorized");
    }
    session.uid = user._key;
    sessionStorage.save(session);
  }
};

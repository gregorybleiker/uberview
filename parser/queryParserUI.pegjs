Expression
    = OrClause 

OrClause =
    left:AndClause _ "OR" _ right:OrClause {
    return `${left}, ${right}`
}
/
AndClause

AndClause =
    left:Subexpression _ "AND" _ right:AndClause {
    return `${left}, ${right}`
}
/
    left:Subexpression _ right:AndClause {
    return `${left}, ${right}`
}
/
Subexpression

Subexpression
    = "(" _ expr:Expression _ ")" { return expr; }
/ Searchword

Searchword "search term"
    = val:[a-z]+ { 
        let joinedVal = val.join("");
        return joinedVal
    }
    
_ "blank"
    = [ \t\n\r] *
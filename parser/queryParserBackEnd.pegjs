Expression
    = OrClause 

OrClause =
    left:AndClause _ "OR" _ right:OrClause {
        return `(for node in union(${left}, ${right}) return node)`;
}
/
AndClause

AndClause =
    left:Subexpression _ "AND" _ right:AndClause {
        return `(for node in intersection(${left}, ${right}) return node)`
}
/
    left:Subexpression _ right:AndClause {
        return `(for node in intersection(${left}, ${right}) return node)`
}
/
Subexpression

Subexpression
    = "(" _ expr:Expression _ ")" { return expr; }
/ Searchword

Searchword "search term"
    = val:[a-z]+ { 
        let joinedVal = val.join("");
        return `
let user = first(
    for u in uberauth_users filter u.username == 'THEUSERNAME' limit 1 return u
)

let tag = first(
    (for ut in outbound user user_relations 
    filter is_same_collection('tags', ut)
    filter ut.name=='${joinedVal}' limit 1 
    return ut)
)

let result = (
    for doc in intersection(
        (for content in outbound tag tagged
        filter is_same_collection('contents', content)
        return content)
        ,
        (for content in outbound user user_relations
        filter is_same_collection('contents', content)
        return content)
    )
    return 
        {   
            name: doc.name, 
            contents: doc.contents,
            tags: ( for t in inbound first(for c in contents filter c.name==doc.name limit 1 return {_id:c._id}) tagged return t.name) 
        }
)
            
return result
            `;
    }
    
_ "blank"
    = [ \t\n\r] *
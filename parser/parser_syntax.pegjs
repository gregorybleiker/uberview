start
  = atomstring 

atomstring
  = left:atom " " right:atomstring { left = left.concat(right); return left; }
    / atom

atom "Atom"
  = chars:[a-zA-Z]+ { return [chars.join("")]; }

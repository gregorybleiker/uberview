# uberview

## Prerequisits
- Docker

## Installation
To see setup options, run
```
cd setup
node installer.js --help
```

## local arangodb dev

`cp -r foxx/api/* /c/ProgramData/ArangoDB-apps/\_db/uberbase/api/`
cp -Recurse -Force .\foxx\api\* c:\ProgramData\ArangoDB-apps\_db\uberbase\api\

### Typescript

Type decorators (@Component etc) are imported via vue-property-decorator which uses vue-class-component
@types in the node_modules dir are automatically imported

## General notes

### nginx

watch out for alias and root in config: with "root", the location gets attached to the root part, with alias, the location starts at the alias location,
so /bla with root /var will resolve to /var/bla
/bla with alias /var will resolve to /var

### code generation

Added hygen for code generation
needs to be installed globally
yarn add hygen -D
afterwards
node_modules/.bin/hygen init self
hygen generator new --name tsvue
customize \_template/tsvue dir
hygen tsvue new --name xyz

### misc

    - Google Fonts self hosted:
    https://jimthedev.com/2016/07/28/using-google-fonts-in-webpack-and-react/

    Arangodb service needs to be linked symbolically so that you can change code during dev
    [need to find out if service needs to be restarted]

    Testing:
    -Nightwatch: Selenium Driver for e2e
    -Karma: Test runner, Javascript Unit tests (no browser)
    -Mocha: Testing framework
    -Cucumber: BDD Tool, integrates with Nightwatch

Babel is not needed if you use typescript (both translate to some javascript)

Karma-typescript package adds typescript support, still have to figure out about webpack

Jasmine is a bdd tool using webdriver.io (vs. nightwatch), vs Cucumber

Sinon: Mocking framework

chai: assertion library

You have to use webpack to compile your sources, you have to set the mime type so that ts is not used as video
added ts-mocha, because typescript tests didn't run (source-map-loader was another option)

### curl test

```shell
curl --header "Content-Type: application/json" \
 --request POST \
 --data '{"username":"xyz","password":"xyz"}' \
 -k https://localhost/auth/signup
```

import * as fs from "fs";

function createContent(nodes: string[]): string {
  let data = "";
  for (let node of nodes) {
    let contentObject = {};
    contentObject["_tag"] = node;
    contentObject["name"] = node;
    data += `${JSON.stringify(contentObject)},\n`;
  }
  return `export default [
    ${data}
  ]`;
}

let file = "./init_try.js";
let data = createContent(["something", "else"]);
fs.writeFileSync(file, data);

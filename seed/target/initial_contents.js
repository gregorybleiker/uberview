"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [{
        "_key": "airplane",
        "name": "airplane",
        "contents": [{
                "label": "Target",
                "content": "http://www.google.ch",
                "type": "text/plain"
            },
            {
                "label": "Title",
                "content": "Airplane",
                "type": "text/plain"
            }
        ]
    }, {
        "_key": "bicycle",
        "name": "bicycle",
        "contents": [{
                "label": "Target",
                "content": "http://www.bing.com",
                "type": "text/plain"
            },
            {
                "label": "Title",
                "content": "Bycicle",
                "type": "text/plain"
            }
        ]
    }, {
        "_key": "train",
        "name": "train",
        "contents": [{
                "label": "Target",
                "content": "http://www.nzz.ch",
                "type": "text/plain"
            },
            {
                "label": "Title",
                "content": "Train",
                "type": "text/plain"
            }
        ]
    }, {
        "_key": "car",
        "name": "car",
        "contents": [{
                "label": "Target",
                "content": "http://www.uberview.io",
                "type": "text/plain"
            },
            {
                "label": "Title",
                "content": "Uberview",
                "type": "text/plain"
            }
        ]
    }, {
        "_key": "boat",
        "name": "boat",
        "contents": [{
                "label": "Target",
                "content": "http://laverna.cc",
                "type": "text/plain"
            },
            {
                "label": "Title",
                "content": "Boat",
                "type": "text/plain"
            }
        ]
    }, {
        "_key": "bus",
        "name": "bus",
        "contents": [{
                "label": "Target",
                "content": "http://www.abc.com",
                "type": "text/plain"
            },
            {
                "label": "Title",
                "content": "Bus",
                "type": "text/plain"
            }
        ]
    }];

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
function createContent(nodes) {
    var data = "";
    for (var _i = 0, nodes_1 = nodes; _i < nodes_1.length; _i++) {
        var node = nodes_1[_i];
        var contentObject = {};
        contentObject["_tag"] = node;
        contentObject["name"] = node;
        data += JSON.stringify(contentObject) + ",\n";
    }
    return "export default [\n    " + data + "\n  ]";
}
var file = "./init_try.js";
var data = createContent(["something", "else"]);
fs.writeFileSync(file, data);

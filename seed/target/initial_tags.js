"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [{ "_key": "land", "name": "land" },
    { "_key": "air", "name": "air" },
    { "_key": "water", "name": "water" },
    { "_key": "pub", "name": "pub" },
    { "_key": "public", "name": "public" },
    { "_key": "private", "name": "private" },
    { "_key": "fast", "name": "fast" },
    { "_key": "slow", "name": "slow" }];

module.exports = {
  presets: [
    [
      "@quasar/babel-preset-app",
      {
        presetEnv: { corejs: 3 }
      }
    ],
    "@vue/cli-plugin-babel/preset"
  ],
  plugins: [
    [
      "transform-imports",
      {
        quasar: {
          transform: "quasar/dist/babel-transforms/imports.js",
          preventFullImport: true
        }
      }
    ]
  ],
  env: {
    test: {
      plugins: [
        [
          "transform-imports",
          {
            quasar: {
              transform: "quasar/dist/babel-transforms/imports.js",
              preventFullImport: false
            }
          }
        ]
      ]
    }
  }
};
